# GUI-Tool For Linux System Administration

Bienvenidos a mi proyecto.

En la carpeta documentacion podreis encontrar todos los archivos que documentan mi proyecto:
- Guia para desarrolladores 
- Guia para usuarios
- Un FAQ (Preguntas y respuestas habituales)
- Una presentacion de diapositivas

Si deseas instalar la herramienta copia el script install_guitool.sh que se encuentra en la carpeta for_developing.
Una vez instalada, reinicia tu terminal y ejecuta guitool_start como root para poder utilizarla o guitool_developing si quieres ponerte a desarrollar!

Marc Serra Hidalgo
isx41745190
EdT - Barcelona
