# Marc Serra
# Install Python3 and PIP
dnf -y install python3
dnf -y install python2-virtualenv

# Create VENV with Python3
mkdir -p /etc/guitool/venv
cd /etc/guitool
virtualenv --python=/usr/bin/python3.5 venv/
source venv/bin/activate

# Create DJANGO project
django-admin startproject GUIT_LinuxAdministration
cd GUIT_LinuxAdministration/
python manage.py migrate
python manage.py runserver

# Create script to run app for use easily (CLIENT)
vim /etc/guitool/start_app.sh
'''
#! /bin/bash
fuser -k 8000/tcp 2> /dev/null
screen -d -m -S DjangoServer bash -c 'python /etc/guitool/GUIT_LinuxAdministration/manage.py runserver 0.0.0.0:8000' 2> /dev/null
firefox http://127.0.0.1:8000  &> /dev/null &
'''

# Create script to run project for developing easily (DEVELOPER)
'''
#!/bin/bash
. cd /etc/guitool/GUIT_LinuxAdministration
fuser -k 8000/tcp 2> /dev/null
firefox http://127.0.0.1:8000  &> /dev/null &
terminator -e 'exec python manage.py runserver 0.0.0.0:8000' &> /dev/null &
'''

# Create aliases for run scripts easily
vim ~/.bashrc
'''
# Edited to run GUITOOL
# For client
alias guitool_start='workon ; /etc/guitool/start_app.sh ; deactivate'
alias workon='source /etc/guitool/activate_env.sh'
# For developing
alias project_dir='cd /etc/guitool/GUIT_LinuxAdministration'
alias guitool_developing='workon ; project_dir; /etc/guitool/start_developing.sh'
'''

# Create superuser for DJANGO admin
python manage.py createsuperuser

# Create user psql
create role marc;
alter user marc with encrypted password 'marc';

# change peer to md5 postgres
cp /var/lib/pgsql/data/pg_hba.conf /var/lib/pgsql/data/pg_hba.conf.bk ; sed 's/peer/md5/g' /var/lib/pgsql/data/pg_hba.conf.bk > /var/lib/pgsql/data/pg_hba.conf

# list screen
screen -ls

# attach screen
screen -x