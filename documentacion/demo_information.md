########################
# User 
########################

## ----------- Good -----------

comprovacions previes: 
cat /etc/passwd | tail -n1
cat /etc/group | grep marc
ls /home/personal/marc

execucio:
marc
kmarc
kmarc
default
ungrup, unaltre
/home/personal/marc
Marc Serra
C/ Sants
680666666

comprovacions finals:
cat /etc/passwd | tail -n3
cat /etc/group | grep marc
ls /home/personal/marc

## Bad: Que el usuario ya exista

## Bad: Error de campo obligatorio (no pos password2)

## Bad: Error de coincidencia de passwords

## Bad: Que la ruta del directorio sea incorrecta

**COMENTAR:**

Si es selecciona el mode DEFAULT per el GRUP PRINCIPAL, aquest usuario pertanyira a un grup que es dira com el seu nom.
Si es selecciona el mode DEFAULT per el DIRECTORI HOME, aquest sera /home/username

########################
# Backup
########################

## ----------- Good ----------- 

comprovacions previes: 
ls ~/Backups_guitool
crontab -l

execucio:
/tmp
Cada semana
Crear ahora

comprovacions finals:
ll ~/Backups_guitool
crontab -l

## Bad: Directorio que no existe

## Bad: Repetir la rutina

**COMENTAR:**

RES

########################
# Docker
########################

## ----------- Good1: Imagen preestablecida ----------- 

comprovacions inicials:
docker images
docker ps -a

execucio:
Opensuse
marc
marc
None
Paquetes basicos
/opt/presentacio
Afegir file
No privileged

comprovacions finals (dintre container):
pwd
ls
vim
cat /etc/os-release | head -n1


## ----------- Good2: Imagen personalizada fedora:28 con subnet bridge y sin directorio trabajo ----------- 

execucio:
Otras: fedora:28
marc2
marc2
bridge
Paquetes redes
no_directorio
Sense file
Privileged

comprovacions finals (dintre container):
pwd
ip a
cat /etc/os-release | head -n1


## Bad: Imagen personalizada que no exista: miimagenjeje

## Bad: Ruta invalida

**COMENTAR:**

Si de base es selecciona la opcio OTRA, sols es podran instalar els paquets si aquesta base personalitzada es de la familia d'alguna de les bases que l'eina porta per defecte (fedora, ubuntu, opensuse, debian).
Si no s'especifica un DIRECTORI DE TREBALL, aquesta automaticament serà /opt/docker. 
Pel contrari: si se n'especifica algun i aquest no existeix es creara sempre i quan sigui una ruta vàlid de sistemes UNIX.
Ah i tampoc permetem crear containers amb el mateix nom.

########################
# BBDD
########################

## ----------- Good1: Crear -----------  

comprovacions previes:
su - postgres
psql
\l

execucio:
instagramgui

usuarios
    id_user (serial - pk)
    username(varchar - unique)
    nombre(varchar - not null)
    edad (integer - not null)

publicaciones
    id_pub(serial - pk)
    texto(varchar) 
    autor (integer - not null)

hashtags
    id_hash(integer - pk)
    texto(text - unique - not null)
    publicacion(integer)

actualizar

FK's:
    taula HASHTAGS camp PUBLICACION --> publicaciones:id_pub
    taula PUBLICACIONES camp AUTOR --> usuario:id_user

user: marc
password: marc
Crear BBDD

comprovacions finals:
\l
\c instagramgui
\d hashtags
\d usuarios
\d publicaciones

## ----------- Good2: Obtenir script ----------- 

Igual - VOLVER A PONER FK
Obtener SCRIPT
Descargar i mirar

## Bad1: paraules reservades

Borrar todo

bbdd

taula1
campo1 campo2

taula2
campo1 select usuarios where true

## Bad2: taula que començi per numero

Borrar todo

bbdd

1taulas
c1 c2

## Bad3: repetir taules

Borrar todo

bbdd

taula
campo1 campo2

taula
campo1 campo2


**COMENTAR:**

Preveu que sols es puguin introduir FK referenciades a un camp que sigui PK o UNIQUE.
Un cop comprovat els errors que s'han demostrat (entre d'altres), en principi ja es pot crear la BBDD.
Tot i aixi, si durant l'execucio salta algun altre tipus d'error (com per exemple que usuari no existeix), un missatge amb aquests sera mostrat en el banner superior.

Si el nom de la BBDD ja existeix, no es produira un error sino que la plataforma es conectara a aquesta. 
Aixo podria haver estat prohibit pero si es deixa aixi ens permet poder afegir taules noves a BBDD que ja existeixen sempre i quant aquestes noves taules no existeixin.


########################
# Buscador
########################

cercar "docker"
cercar "a"

**COMENTAR:**

que si dona temps l'explicare com ho he fet o no

















########################
# Coses a preparar a PC profe antes de presentació
########################

## executar script instalacio

bash install.sh

## instalar psql

dnf install postgresql postgresql-server

postgresql-setup --initdb

systemctl start postgresql

su - postgres

psql

create role marc;

create user marc;

alter user marc with encrypted password 'marc';

ALTER ROLE marc WITH SUPERUSER;

ALTER ROLE marc WITH LOGIN;

ControlD

ControlD

cp /var/lib/pgsql/data/pg_hba.conf /var/lib/pgsql/data/pg_hba.conf.bk ; sed 's/peer/md5/g' /var/lib/pgsql/data/pg_hba.conf.bk > /var/lib/pgsql/data/pg_hba.conf


## instalar sublime i preparar demo_information a una pantalla i obrir documentacions a pestanyes

rpm -v --import https://download.sublimetext.com/sublimehq-rpm-pub.gpg

dnf config-manager --add-repo https://download.sublimetext.com/rpm/stable/x86_64/sublime-text.repo

dnf -y update

dnf -y install sublime-text


## preparar diapos

firefox /etc/guitool/documentacion/presentacion/presentacion.html &

cd /etc/guitool ; subl .

