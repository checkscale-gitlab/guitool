#! /bin/bash
# Script para pasar un fichero de markdown a html
#----------------------------------------------------------

pandoc \
	--standalone \
	--to=dzslides \
	--incremental \
	--css=estil.css \
	--output=presentacion.html \
	presentacion.md